// blocky.vert - an GLSL vertex shader with animation
// the App updates uniforms \"slowly\" (eg once per frame) for animation.
uniform float Brick2Size;
uniform double brickNoiseScale;
const vec3 LightPosition = vec3(0.0, 0.0, 4.0);
const float BlockScale = 0.30;
// varyings are written by vert shader, interpolated, and read by frag shader.
varying float LightIntensity;
varying vec2  BlockPosition;

varying vec2 v_texCoord2D;
varying vec3 v_texCoord3D;
varying vec4 v_color;

void main(void)
{
	v_color = gl_Color;  
	v_texCoord2D = gl_MultiTexCoord0.xy;
	v_texCoord3D = gl_Vertex.xyz * brickNoiseScale;

    // per-vertex diffuse lighting
    vec4 ecPosition    = gl_ModelViewMatrix * gl_Vertex;
    vec3 tnorm         = normalize(gl_NormalMatrix * gl_Normal);
    vec3 lightVec      = normalize(LightPosition - vec3 (ecPosition));
    LightIntensity     = max(dot(lightVec, tnorm), 0.0);
    // blocks will be determined by fragment's position on the XZ plane.
    BlockPosition  = gl_Vertex.xz / BlockScale;
    // scale the geometry based on an animation variable.
    vec4 vertex    = gl_Vertex;
    //vertex.w       = 1.0 + 0.4 * (Brick2Size + 1.0);
    gl_Position    = gl_ModelViewProjectionMatrix * vertex;
}
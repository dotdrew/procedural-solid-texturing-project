// blocky.frag - an GLSL fragment shader with animation
// the App updates uniforms \"slowly\" (eg once per frame) for animation.
uniform float BlockySize;
const vec3 Color1 = vec3(1.0, 1.0, 1.0);
const vec3 Color2 = vec3(0.0, 0.0, 0.0);
// varyings are written by vert shader, interpolated, and read by frag shader.
varying vec2  BlockPosition;
varying float LightIntensity;
void main(void)
{
    vec3 color;
    float ss, tt, w, h;
    ss = BlockPosition.x;
    tt = BlockPosition.y;
    if (fract(tt * 0.5) > 0.5)
        ss += 0.5;
    ss = fract(ss);
    tt = fract(tt);

    float blockFract = (BlockySize + 1.1) * 0.4;
    w = step(ss, blockFract);
    h = step(tt, blockFract);
    color = mix(Color2, Color1, w * h) * LightIntensity;
    gl_FragColor = vec4 (color, 1.0);
}
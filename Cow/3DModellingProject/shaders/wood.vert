varying vec2 v_texCoord2D;
varying vec3 v_texCoord3D;
varying vec4 v_color;

const vec3 LightPosition = vec3(0.0, 0.0, 4.0);
varying float LightIntensity;

uniform float WoodSize;

//uniform double simplexNoiseScale;

void main()
{	
	vec4 ecPosition    = gl_ModelViewMatrix * gl_Vertex;
    vec3 tnorm         = normalize(gl_NormalMatrix * gl_Normal);
    vec3 lightVec      = normalize(LightPosition - vec3 (ecPosition));
    LightIntensity     = max(dot(lightVec, tnorm), 0.0);

	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
	v_texCoord2D = gl_MultiTexCoord0.xy;
	v_texCoord3D = gl_Vertex.xyz; // * simplexNoiseScale; //5.0; //0.05;
	v_color = gl_Color;  
}

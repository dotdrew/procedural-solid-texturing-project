// blocky.vert - an GLSL vertex shader with animation
// the App updates uniforms \"slowly\" (eg once per frame) for animation.
uniform float BlockySize;
const vec3 LightPosition = vec3(0.0, 0.0, 4.0);
const float BlockScale = 0.30;
// varyings are written by vert shader, interpolated, and read by frag shader.
varying float LightIntensity;
varying vec2  BlockPosition;
void main(void)
{
    // per-vertex diffuse lighting
    vec4 ecPosition    = gl_ModelViewMatrix * gl_Vertex;
    vec3 tnorm         = normalize(gl_NormalMatrix * gl_Normal);
    vec3 lightVec      = normalize(LightPosition - vec3 (ecPosition));
    LightIntensity     = max(dot(lightVec, tnorm), 0.0);
    // blocks will be determined by fragment's position on the XZ plane.
    BlockPosition  = gl_Vertex.xz / BlockScale;
    // scale the geometry based on an animation variable.
    vec4 vertex    = gl_Vertex;
    //vertex.w       = 1.0 + 0.4 * (BlockySize + 1.0);
    gl_Position    = gl_ModelViewProjectionMatrix * vertex;
}
//
// multitex.frag - simple fragment shader for multitexturing terrain
//


varying float LightIntensity; 
varying vec4 VertexColor;
varying vec2 TexCoord0;
varying vec2 TexCoord1;

uniform sampler2D Tex0;
uniform sampler2D Tex1;

void main (void)
{
	vec4 texcol0 = texture2D( Tex0, TexCoord0 );
	vec4 texcol1 = texture2D( Tex1, TexCoord1 );

	vec4 texColor = texcol0 * VertexColor.x + texcol1 * VertexColor.y;

    gl_FragColor = texColor * LightIntensity;

}

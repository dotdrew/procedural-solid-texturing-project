varying vec2 v_texCoord2D;
varying vec3 v_texCoord3D;
varying vec4 v_color;

uniform double simplexNoiseScale;

void main()
{	
  gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
  v_texCoord2D = gl_MultiTexCoord0.xy;
  v_texCoord3D = gl_Vertex.xyz * simplexNoiseScale; //5.0; //0.05;
  v_color = gl_Color;  
}

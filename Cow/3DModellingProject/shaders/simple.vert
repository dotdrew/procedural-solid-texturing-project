void main(void)
{
   vec4 a = gl_Vertex;
   a.x = a.x * 0.5; //halves x-dimension of shape
   //a.y = a.y * 0.5;


   gl_Position = gl_ModelViewProjectionMatrix * a;

}      
#include "stdafx.h"
using namespace std;

osg::Switch *pGeoSwitch;
osg::Switch *pStateSetSwitch;

osg::Geode* shapeToBeChanged;
osg::Group* shaderToBeChanged;

osg::Geode* createCube();
osg::Geode* createSphere();
osg::Geode* createCylinder();
osg::Geode* createCone();
osg::Geode* createCapsule();

osg::Group* createSimplex();
osg::Group* createBlocky();
osg::Group* createBrick2();
osg::Group* createSpot();
osg::Group* createWood();

osg::Group* simplexNoiseSSGroup;
osg::Group* blockySSGroup;
osg::Group* Brick2SSGroup;
osg::Group* spotSSGroup;
osg::Group* woodSSGroup;


int shapeVisible = 0;
int currentShader = 0;

float cubeScale = 1.0f;
float sphereScale = 1.0f;
float cylinderScale = 1.0f;
float coneScale = 1.0f;
float capsuleScale = 1.0f;

double pParamW = 7.0;
double simNoisScaleVar = 1.0;
float blockySizeVar = 1.0f;
float brickSizeVar = 1.0f;
double brickNoisScaleVar = 50.0;
float woodNoiseVar = 0.2f;
float woodRingVar = 5.0f;

double PCFreq = 0.0;
__int64 CounterStart = 0;

void LoadShaderSource( osg::Shader* shader, const std::string& fileName )
{
	// taken from the OpenSceneGraph shader sample
	std::string fqFileName = osgDB::findDataFile(fileName);
	if( fqFileName.length() != 0 )
	{
		shader->loadShaderSourceFromFile( fqFileName.c_str() );
	}
	else
	{
		osg::notify(osg::WARN) << "File \"" << fileName << "\" not found." << std::endl;
	}
}

class myKeyboardEventHandler : public osgGA::GUIEventHandler
{
	public:
		virtual bool handle(const osgGA::GUIEventAdapter& ea,osgGA::GUIActionAdapter&);
		virtual void accept(osgGA::GUIEventHandlerVisitor& v)   { v.visit(*this); };
};

bool myKeyboardEventHandler::handle(const osgGA::GUIEventAdapter& ea,osgGA::GUIActionAdapter& aa)
{
	switch(ea.getEventType())
	{
		case(osgGA::GUIEventAdapter::KEYDOWN):
		{
			switch(ea.getKey())
			{
			case '1':
				std::cout << " 1 key pressed, Cube selected" << std::endl;
				pGeoSwitch->setSingleChildOn(0); //sets the current geometry rendered to first index of switch
				shapeVisible = 0; //integer used to keep track of which geometry was last selected
				return false;
				break;
			case '2':
				std::cout << " 2 key pressed, Sphere selected" << std::endl;
				pGeoSwitch->setSingleChildOn(1);
				shapeVisible = 1;
				return false;
				break;
			case '3':
				std::cout << " 3 key pressed, Cylinder selected" << std::endl;
				pGeoSwitch->setSingleChildOn(2);
				shapeVisible = 2;
				return false;
				break;
			case '4':
				std::cout << " 4 key pressed, Cone selected" << std::endl;
				pGeoSwitch->setSingleChildOn(3);
				shapeVisible = 3;
				return false;
				break;
			case '5':
				std::cout << " 5 key pressed, Capsule selected" << std::endl;
				pGeoSwitch->setSingleChildOn(4);
				shapeVisible = 4;
				return false;
				break;
			case 'q':
				std::cout << " Q key pressed, Simplex Noise shader selected" << std::endl;
				pStateSetSwitch->setSingleChildOn(0); //sets the current shader rendered to first index of switch
				currentShader = 0; //integer used to keep track of which shader was last selected
				return false;
				break;
			case 'w':
				std::cout << " W key pressed, Blocky shader selected" << std::endl;
				pStateSetSwitch->setSingleChildOn(1);
				currentShader = 1;
				return false;
				break;
			case 'e':
				std::cout << " E key pressed, Brick shader selected" << std::endl;
				pStateSetSwitch->setSingleChildOn(2);
				currentShader = 2;
				return false;
				break;
			case 'r':
				std::cout << " R key pressed, Wood shader selected" << std::endl;
				pStateSetSwitch->setSingleChildOn(3);
				currentShader = 3;
				return false;
				break;
			case 't':
				std::cout << " T key pressed, Spot shader selected" << std::endl;
				pStateSetSwitch->setSingleChildOn(4);
				currentShader = 4;
				return false;
				break;
			case 'a':
				std::cout << " A key pressed" << std::endl;
				pGeoSwitch->removeChildren(shapeVisible, 1); //removes the current geometry from geometry switch
				shapeToBeChanged = new osg::Geode(); //creates a new empty Geode
				switch(shapeVisible) //checks which shape is currently being rendered
				{
				case 0: //this case is the Cube
					if(cubeScale >= 0.1) //if the cube is not too small
						cubeScale -= 0.01; //then decrement the scale, this will not occur when the cube is tiny
						shapeToBeChanged = createCube(); //creates a new cube at the new scale and adds it to the empty geode
					break;
				case 1: //Repeat of above code but for each individual shape
					if(sphereScale >= 0.1)
						sphereScale -= 0.01;
						shapeToBeChanged = createSphere();
					break;
				case 2:
					if(cylinderScale >= 0.1)
						cylinderScale -= 0.01;
						shapeToBeChanged = createCylinder();
					break;
				case 3:
					if(coneScale >= 0.1)
						coneScale -= 0.01;
						shapeToBeChanged = createCone();
					break;
				case 4:
					if(capsuleScale >= 0.1)
						capsuleScale -= 0.01;
						shapeToBeChanged = createCapsule();
					break;
				}
				pGeoSwitch->insertChild(shapeVisible, shapeToBeChanged); //inserts the new geode at the same index of the original geode
				pGeoSwitch->setSingleChildOn(shapeVisible); //sets the shape being rendered to the same index so as to refresh the rendered shape
				return false;
				break;
			case 's':
				std::cout << " S key pressed" << std::endl; //same code as case a, but increasing in size
				pGeoSwitch->removeChildren(shapeVisible, 1);
				shapeToBeChanged = new osg::Geode();
				switch(shapeVisible)
				{
				case 0:
					cubeScale += 0.01;
					shapeToBeChanged = createCube();
					break;
				case 1:
					sphereScale += 0.01;
					shapeToBeChanged = createSphere();
					break;
				case 2:
					cylinderScale += 0.01;
					shapeToBeChanged = createCylinder();
					break;
				case 3:
					coneScale += 0.01;
					shapeToBeChanged = createCone();
					break;
				case 4:
					capsuleScale += 0.01;
					shapeToBeChanged = createCapsule();
					break;
				}
				pGeoSwitch->insertChild(shapeVisible, shapeToBeChanged);
				pGeoSwitch->setSingleChildOn(shapeVisible);
				return false;
				break;
			case 'd': //works in the same way case a, but refreshes the current shader being rendered not geometry
				std::cout << " D key pressed" << std::endl;
				pStateSetSwitch->removeChildren(currentShader, 1);
				shaderToBeChanged = new osg::Group();
				switch(currentShader)
				{
				case 0:
					if(pParamW >= 0.1)
						pParamW -= 0.01;
						shaderToBeChanged = createSimplex();
					break;
				case 1:
					if(blockySizeVar >= 0.1)
						blockySizeVar -= 0.01;
						shaderToBeChanged = createBlocky();
					break;
				case 2:
					if(brickSizeVar >= 0.1)
						brickSizeVar -= 0.01;
						shaderToBeChanged = createBrick2();
					break;
				case 3:
					if(woodNoiseVar >= 0.01)
						woodNoiseVar -= 0.01;
						shaderToBeChanged = createWood();
					break;
				case 4:
					shaderToBeChanged = createSpot(); 
					break;
				default:
					break;
				}
				pStateSetSwitch->insertChild(currentShader, shaderToBeChanged);
				shaderToBeChanged->addChild(pGeoSwitch);
				pStateSetSwitch->setSingleChildOn(currentShader);
				return false;
				break;
			case 'f': //same as case d, but scaling is incremented
				std::cout << " F key pressed" << std::endl;
				pStateSetSwitch->removeChildren(currentShader, 1);
				shaderToBeChanged = new osg::Group();
				switch(currentShader)
				{
				case 0:
					pParamW += 0.01;
					shaderToBeChanged = createSimplex();
					break;
				case 1:
					if(blockySizeVar <= 1.35)
						blockySizeVar += 0.01;
						shaderToBeChanged = createBlocky();
					break;
				case 2:
					if(brickSizeVar <= 1.35)
						brickSizeVar += 0.01;
						shaderToBeChanged = createBrick2();
					break;
				case 3:
					if(woodNoiseVar <= 1.0)
						woodNoiseVar += 0.01;
						shaderToBeChanged = createWood();
					break;
				case 4:
					shaderToBeChanged = createSpot();
					break;
				}
				pStateSetSwitch->insertChild(currentShader, shaderToBeChanged);
				shaderToBeChanged->addChild(pGeoSwitch);
				pStateSetSwitch->setSingleChildOn(currentShader);
				break;
			case 'g': //same as case d but with different variables
				std::cout << " G key pressed" << std::endl;
				pStateSetSwitch->removeChildren(currentShader, 1);
				shaderToBeChanged = new osg::Group();
				switch(currentShader)
				{
				case 0:
					if(simNoisScaleVar > 0.1)
						simNoisScaleVar -= 0.01;
						shaderToBeChanged = createSimplex();
					break;
				case 1:
					shaderToBeChanged = createBlocky();
					break;
				case 2:
					if(brickNoisScaleVar > 1.0)
						brickNoisScaleVar -= 0.1;
						shaderToBeChanged = createBrick2();
					break;
				case 3:
					if(woodRingVar > 2.0)
						woodRingVar -= 0.5;
						shaderToBeChanged = createWood();
					break;
				case 4:
					shaderToBeChanged = createSpot();
					break;
				}
				pStateSetSwitch->insertChild(currentShader, shaderToBeChanged);
				shaderToBeChanged->addChild(pGeoSwitch);
				pStateSetSwitch->setSingleChildOn(currentShader);
				break;
			case 'h': //same as case g but incrementing variables not decrementing
				std::cout << " H key pressed" << std::endl;
				pStateSetSwitch->removeChildren(currentShader, 1);
				shaderToBeChanged = new osg::Group();
				switch(currentShader)
				{
				case 0:
					simNoisScaleVar += 0.01;
					shaderToBeChanged = createSimplex();
					break;
				case 1:
					shaderToBeChanged = createBlocky();
					break;
				case 2:
					brickNoisScaleVar += 0.1;
					shaderToBeChanged = createBrick2();
					break;
				case 3:
					woodRingVar += 0.5;
					shaderToBeChanged = createWood();
					break;
				case 4:
					shaderToBeChanged = createSpot();
					break;
				}
				pStateSetSwitch->insertChild(currentShader, shaderToBeChanged);
				shaderToBeChanged->addChild(pGeoSwitch);
				pStateSetSwitch->setSingleChildOn(currentShader);
				break;
			default:
				return false;
			} 
		}
		default:
			return false;
	}
}

osg::StateSet* createStateSet( string shaderName )
{
	// create a program and attach fragment and vertex shaders to it
	osg::Program *ShaderProgram = new osg::Program;
	ShaderProgram->setName( "multitex" );
	osg::Shader *SPVertObj = new osg::Shader( osg::Shader::VERTEX );
	osg::Shader *SPFragObj = new osg::Shader( osg::Shader::FRAGMENT );

	ShaderProgram->addShader( SPFragObj );
	ShaderProgram->addShader( SPVertObj );
	
	//Working shaders: simple, simple2, blocky, simplexNoise, Brick2, frag5
	LoadShaderSource( SPVertObj, "shaders\\" + shaderName + ".vert" ); 
	LoadShaderSource( SPFragObj, "shaders\\" + shaderName + ".frag" ); 

	// create a simple material, and assign it to the geometry
	osg::Material *pMaterial = new osg::Material();
	pMaterial->setAmbient( osg::Material::FRONT, osg::Vec4( 0.2, 0.2, 0.2, 1.0 ) );
	pMaterial->setDiffuse( osg::Material::FRONT, osg::Vec4( 0.8, 0.8, 0.8, 1.0 ) );

	osg::StateSet* pStateSet = new osg::StateSet();
	pStateSet->setAttribute( pMaterial );
	
	pStateSet->setAttributeAndModes( ShaderProgram, osg::StateAttribute::ON); 

	//uniform variable for use in simplexNoise.frag and Brick2.frag
	const osg::Vec4 pParamVec4 = osg::Vec4( 17.0*17.0, 34.0, 1.0, pParamW );  
	osg::Uniform* p_Param = new osg::Uniform( "pParam", pParamVec4 );    
	pStateSet->addUniform( p_Param );

	osg::Uniform* simplexNoiseScale = new osg::Uniform( "simplexNoiseScale", simNoisScaleVar);
	pStateSet->addUniform( simplexNoiseScale );

	osg::Uniform* p_time = new osg::Uniform( "time", 0.0f ); //this could be set to a time counter to animate shaders
	pStateSet->addUniform( p_time );

	//uniform variable for blocky.frag
	osg::Uniform* BlockySizeUniform   = new osg::Uniform( "BlockySize", blockySizeVar ); //1.0f
    pStateSet->addUniform( BlockySizeUniform );

	//uniform variable for Brick2.frag
	osg::Uniform* BrickSizeUniform   = new osg::Uniform( "Brick2Size", brickSizeVar ); //1.0f
    pStateSet->addUniform( BrickSizeUniform );

	osg::Uniform* brickNoiseScale = new osg::Uniform( "brickNoiseScale", brickNoisScaleVar);
	pStateSet->addUniform( brickNoiseScale );

	//uniform variables for wood shader
	osg::Uniform* WoodNoise  = new osg::Uniform( "WoodNoise", woodNoiseVar ); //1.0f
    pStateSet->addUniform( WoodNoise );

	osg::Uniform* woodRingScale  = new osg::Uniform( "woodRingScale", woodRingVar ); //50.0f
    pStateSet->addUniform( woodRingScale );

	return pStateSet;
}

 // function for creating shape geometry and returns a geode containing that shape
osg::Geode* createCube()
{
	osg::ShapeDrawable* cubeDrawable = new osg::ShapeDrawable(new osg::Box(osg::Vec3(0,0,0), cubeScale));
	osg::Geode* cubeGeode = new osg::Geode();
	cubeGeode->addDrawable(cubeDrawable);
	
	return cubeGeode;
}

osg::Geode* createSphere()
{
	osg::ShapeDrawable* sphereDrawable = new osg::ShapeDrawable(new osg::Sphere(osg::Vec3(0,0,0), 0.5f * sphereScale));
	osg::Geode* sphereGeode = new osg::Geode();
	sphereGeode->addDrawable(sphereDrawable);
	
	return sphereGeode;
}

osg::Geode* createCylinder()
{
	osg::ShapeDrawable* cylinderDrawable = new osg::ShapeDrawable(new osg::Cylinder(osg::Vec3(0,0,0), 0.5f * cylinderScale, cylinderScale));
	osg::Geode* cylinderGeode = new osg::Geode();
	cylinderGeode->addDrawable(cylinderDrawable);
	
	return cylinderGeode;
}

osg::Geode* createCone()
{
	osg::ShapeDrawable* coneDrawable = new osg::ShapeDrawable(new osg::Cone(osg::Vec3(0,0,0), 0.5f * coneScale, coneScale));
	osg::Geode* coneGeode = new osg::Geode();
	coneGeode->addDrawable(coneDrawable);
	
	return coneGeode;
}

osg::Geode* createCapsule()
{
	osg::ShapeDrawable* capsuleDrawable = new osg::ShapeDrawable(new osg::Capsule(osg::Vec3(0,0,0), 0.5f * capsuleScale, capsuleScale));
	osg::Geode* capsuleGeode = new osg::Geode();
	capsuleGeode->addDrawable(capsuleDrawable);
	
	return capsuleGeode;
}

// code for creating shader state set, returns that state set as a group
osg::Group* createSimplex()
{
	osg::StateSet* simplexNoiseStateSet = createStateSet( "simplexNoise" );
	simplexNoiseSSGroup = new osg::Group;
	simplexNoiseSSGroup->setStateSet(simplexNoiseStateSet);

	return simplexNoiseSSGroup;
}

osg::Group* createBlocky()
{
	osg::StateSet* blockyStateSet = createStateSet( "blocky" );
	blockySSGroup = new osg::Group;
	blockySSGroup->setStateSet(blockyStateSet);

	return blockySSGroup;
}

osg::Group* createBrick2()
{
	osg::StateSet* Brick2StateSet = createStateSet( "Brick2" );
	Brick2SSGroup = new osg::Group;
	Brick2SSGroup->setStateSet(Brick2StateSet);
	
	return Brick2SSGroup;
}

osg::Group* createWood()
{
	osg::StateSet* woodStateSet = createStateSet( "wood" );
	woodSSGroup = new osg::Group;
	woodSSGroup->setStateSet(woodStateSet);
	
	return woodSSGroup;
}

osg::Group* createSpot()
{
	osg::StateSet* spotStateSet = createStateSet( "spot" );
	spotSSGroup = new osg::Group;
	spotSSGroup->setStateSet(spotStateSet);
	
	return spotSSGroup;
}


osg::Group* createSceneGraph( )
{
	// create the root node of the scene graph
	osg::Group* group = new osg::Group;
	
	//create StateSet switch node
	pStateSetSwitch = new osg::Switch;

	//create geometry switch node
	pGeoSwitch = new osg::Switch; 

	/*
	osgText::Text* shapeText = new osgText::Text(); 
	osg::Geode* textGeode = new osg::Geode();
	group->addChild(textGeode);
	shapeText->setFont("");
	shapeText->setPosition(osg::Vec3(-2.0f, 2.0f, 0.0f));
	shapeText->setText("Cube shape selected");
	shapeText->setCharacterSize(0.1f);
	textGeode->addDrawable(shapeText);
	*/
	
	//add StateSet switch node as only child of group node
	group->addChild( pStateSetSwitch ); 

	// add each geode node to the geometry switch node as a child
	pGeoSwitch->addChild(createCube());
	pGeoSwitch->addChild(createSphere());
	pGeoSwitch->addChild(createCylinder());
	pGeoSwitch->addChild(createCone());
	pGeoSwitch->addChild(createCapsule());

	// add StateSet groups as children to StateSet switch node
	pStateSetSwitch->addChild(createSimplex()); //simplex
	pStateSetSwitch->addChild(createBlocky());
	pStateSetSwitch->addChild(createBrick2());
	pStateSetSwitch->addChild(createWood());
	pStateSetSwitch->addChild(createSpot());

	// add geo switch as child of each state set group
	simplexNoiseSSGroup->addChild(pGeoSwitch);
	blockySSGroup->addChild(pGeoSwitch);
	Brick2SSGroup->addChild(pGeoSwitch);
	woodSSGroup->addChild(pGeoSwitch);
	spotSSGroup->addChild(pGeoSwitch);
	
	// set only one geometry geode node and StateSet node as active (Cube and simplexNoise)
	pGeoSwitch->setSingleChildOn(0);
	pStateSetSwitch->setSingleChildOn(0);

	return group;
}

void StartCounter() //starts a high frequency timer
{
	LARGE_INTEGER li;
	if(!QueryPerformanceFrequency(&li))
		cout << "QueryPerformanceCounter failed!\n";

	PCFreq = double(li.QuadPart)/1000.0;

	QueryPerformanceCounter(&li);
	CounterStart = li.QuadPart;
}

double GetCounter() //returns the current count of the high frequency timer
{
	LARGE_INTEGER li;
	QueryPerformanceCounter(&li);
	return (double(li.QuadPart-CounterStart)/PCFreq);
}

int main( int argc, char *argv[] )
{
	// construct a viewer.
	osgViewer::Viewer viewer;

	// configure the view to render in a window
	viewer.apply( new osgViewer::SingleWindow( 50, 50, 1280, 720 ) );

	// use the OSG trackball manipulator to control the camera
	viewer.setCameraManipulator( new osgGA::TrackballManipulator() );

	// create a scene graph and associate it with the viewer
	viewer.setSceneData( createSceneGraph( ) );

	// change the dafault lighting set up to a sky light (it defaults to 'headlight')
	viewer.getCamera()->getView()->setLightingMode(osg::View::NO_LIGHT);

	// add keyboard event handler to viewer
	myKeyboardEventHandler* myFirstEventHandler = new myKeyboardEventHandler();
	viewer.addEventHandler(myFirstEventHandler);

	// run the viewer
	viewer.run();
	
	/*
	//code designed for testing, runs through several shaders on one model and writes frame time per frame
	//to files for each shader, these can then be used for analysis
	int numFrames = 0;
	ofstream simplexFile, blockyFile, brickFile, woodFile, fragFile;
	simplexFile.open("testing\\simplex - Copy (4).txt");
	blockyFile.open("testing\\blocky - Copy (4).txt");
	brickFile.open("testing\\brick - Copy (4).txt");
	woodFile.open("testing\\wood - Copy (4).txt");
	spotFile.open("testing\\frag5 - Copy (4).txt"); //renamed this shader

	while(!viewer.done() )
	{
		while(numFrames < 50)
		{
			StartCounter();
			viewer.frame();
			simplexFile << GetCounter() << "\n";
			numFrames++;
		}
		simplexFile.close();
		pStateSetSwitch->setSingleChildOn(1);
		while(numFrames >= 50 && numFrames <100)
		{
			StartCounter();
			viewer.frame();
			blockyFile << GetCounter() << "\n";
			numFrames++;
		}
		blockyFile.close();
		pStateSetSwitch->setSingleChildOn(2);
		while(numFrames >= 100 && numFrames <150)
		{
			StartCounter();
			viewer.frame();
			brickFile << GetCounter() << "\n";
			numFrames++;
		}
		brickFile.close();
		pStateSetSwitch->setSingleChildOn(5);
		while(numFrames >= 150 && numFrames <200)
		{
			StartCounter();
			viewer.frame();
			woodFile << GetCounter() << "\n";
			numFrames++;
		}
		woodFile.close();
		pStateSetSwitch->setSingleChildOn(3);
		while(numFrames >= 200 && numFrames <250)
		{
			StartCounter();
			viewer.frame();
			spotFile << GetCounter() << "\n";
			numFrames++;
		}
		spotFile.close();
		if(numFrames == 250)
		{
			return 0;
		}
	}
	
	*/
	return 0;
}
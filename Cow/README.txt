This program is designed to show off procedural solid textures.

Some shaders have been influenced by open source shaders, with references where appropriate.

Keys:
1 - 5, changes shape between a Cube, Sphere, Cylinder, Cone and Capsule respectively.
Q - Y, changes shaders between simplex noise, blocks, brick, spots and wood respectively
A + S, makes the current shape smaller or bigger respectively 
D + F, decrement and increment shader variables, it depends on the shader which variables
G + H, same as above

Mouse:
Left-click and move: Rotate around object
Scroll: Zoom in or out
Right-click and move up-down: Zoom in or out also
Middle-click and move: Move camera 